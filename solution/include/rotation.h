//
// Created by Александр on 31.12.2021.
//

#ifndef LAB_ROTATION_ROTATION_H
#define LAB_ROTATION_ROTATION_H

struct image rotate( struct image const source );

#endif //LAB_ROTATION_ROTATION_H
