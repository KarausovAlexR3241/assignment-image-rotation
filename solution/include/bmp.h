//
// Created by Александр on 30.12.2021.
//

#ifndef LAB_ROTATION_BMP_H
#define LAB_ROTATION_BMP_H
#include <img.h>
#include <stdio.h>
#include <stdint.h>


#pragma pack(push, 1)
struct bmp_header
{
    uint16_t bfType;
    uint32_t  bfileSize;
    uint32_t bfReserved;
    uint32_t bOffBits;
    uint32_t biSize;
    uint32_t biWidth;
    uint32_t  biHeight;
    uint16_t  biPlanes;
    uint16_t biBitCount;
    uint32_t biCompression;
    uint32_t biSizeImage;
    uint32_t biXPelsPerMeter;
    uint32_t biYPelsPerMeter;
    uint32_t biClrUsed;
    uint32_t biClrImportant;
};
#pragma pack(pop)


/*  deserializer   */
enum read_status  {
    READ_OK,
    READ_INVALID_SIGNATURE,
    READ_INVALID_BITS,
    READ_INVALID_HEADER
    /* коды других ошибок  */
};


enum read_status from_bmp( FILE* in, struct image* img );


/*  serializer   */
enum  write_status  {
    WRITE_OK,
    WRITE_ERROR,
    
    /* коды других ошибок  */
};


enum write_status to_bmp( FILE* out, struct image const* img );

#endif //LAB_ROTATION_BMP_H
