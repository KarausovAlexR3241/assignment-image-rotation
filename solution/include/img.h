//
// Created by Александр on 30.12.2021.
//

#ifndef LAB_ROTATION_IMG_H
#define LAB_ROTATION_IMG_H
#include <stdio.h>
#include <stdint.h>

struct pixel { uint8_t b, g, r; };

struct image {
    uint64_t width, height;
    struct pixel* data;
};

#endif //LAB_ROTATION_IMG_H
