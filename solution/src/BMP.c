//
// Created by Александр on 31.12.2021.
//

#include <bmp.h>
#include <img.h>
#include <stdio.h>
#include <stdlib.h>

struct bmp_header;
enum read_status from_bmp( FILE* in, struct image* img ){
    //создаем и считываем заголовок
    struct bmp_header head= {0};
    
    if ((fread( &head, sizeof(struct bmp_header), 1, in))!=1){
         return READ_INVALID_HEADER;
    }
   
    //задаем уже считанные параметры
    img->data = NULL;
    img->data = malloc(sizeof(struct pixel)*(head.biWidth)*(head.biHeight));
    
    if ((img->data)==NULL){
        printf("Cannot allocate memory\n");
        exit(1);
    }

    img->width = head.biWidth;
    img->height = head.biHeight;


    


    for(uint32_t i=0; i< head.biHeight; i++) {
        if(fread(&(img->data[i*img->width]), sizeof(struct pixel), head.biWidth, in)!=head.biWidth){
          return READ_INVALID_BITS;
        }
        if (fseek( in, (head.biWidth)%4, SEEK_CUR)){
          return READ_INVALID_BITS;
        }
    }


    return READ_OK;

}

enum write_status to_bmp( FILE* out, struct image const* img ){
    // зоздаем новый заголовок
  struct pixel pix;
  struct bmp_header head={
  
 .bfType = 0x4D42,
  .bfileSize = sizeof(struct bmp_header)+((img->width * sizeof(struct pixel) + ((4 - (img->width * sizeof(struct pixel))) % 4)) * img->height),
  .bfReserved = 0,
  .bOffBits = sizeof(struct bmp_header),
  .biSize = 40,
  .biWidth = img->width,
  .biHeight = img->height,
  .biPlanes = 1,
  .biBitCount = 24,
  .biCompression = 0,
  .biSizeImage = (img->width * sizeof(struct pixel) + ((4 - (img->width * sizeof(struct pixel))) % 4)) * img->height,
  .biXPelsPerMeter = 0,
  .biYPelsPerMeter = 0,
  .biClrUsed = 0,
  .biClrImportant = 0
};
if (fwrite(&head, sizeof(struct bmp_header), 1, out)!=1){
  return WRITE_ERROR;
}

  if (fseek(out, head.bOffBits, SEEK_SET)){
    return WRITE_ERROR;
  }
  //Создаем счетчики и мусорный байт
  uint8_t Padding = (4 - (sizeof(struct pixel)* head.biWidth)) % 4;
  uint64_t x=0;
  uint64_t y=0;
  while (x<img->height) {
    while (y<img->width) {
      pix = img->data[img->width * x + y];
      if (fwrite(&pix, sizeof(struct pixel), 1, out)!=1){
        return WRITE_ERROR;
      }
      y=y+1;
    }
    x=x+1;
    y=0;
    if (Padding != 0) {
     uint8_t i=0;
      while (i<Padding) {
        i=i+1;
        if (fwrite(&pix, sizeof(char), 1, out)!=1){
          return WRITE_ERROR;
        }
      }
    }else return WRITE_ERROR;
  }
  return WRITE_OK;

}
