//
// Created by Александр on 31.12.2021.
//
#include "img.h"
#include "rotation.h"
#include <stdlib.h>
struct image rotate( struct image const source ) {
    //задаются параметры нового изображения(выделяется память)
    struct image rotated_img = {0};
    rotated_img.width = source.height;
    rotated_img.height = source.width;
    rotated_img.data= malloc((sizeof(struct pixel))*rotated_img.width*rotated_img.height);
    if ((rotated_img.data)==NULL){
        printf("Cannot allocate memory\n");
        exit(1);
    }

    
    
   
    //информация из первоночальной data тоже меняется относительно новой ширины и высоты
    for (uint64_t slide_width = 0; slide_width < source.width; slide_width++) {
        for (uint64_t slide_height = 0; slide_height < source.height; slide_height++) {
            rotated_img.data[rotated_img.width*(slide_width+1) -(1+slide_height)] =  source.data[slide_height*source.width + slide_width];
        }
    }
    return rotated_img;
}
