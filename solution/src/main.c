#include "bmp.h"
#include "img.h"
#include "rotation.h"
#include <stdlib.h>


int main( int argc, char** argv ) {
    (void) argc; (void) argv; // supress 'unused parameters' warning
    struct image initial_image, rotated_image;
    // Чтение
    FILE *first_bmp= fopen(argv[1],"rb");

    from_bmp(first_bmp, &initial_image);
    rotated_image= rotate(initial_image);
    // Запись
    FILE *second_bmp=fopen(argv[2],"wb");
    to_bmp(second_bmp, &rotated_image);

    // Закрытия файлов и очистка памяти от действий функции random
    fclose(first_bmp);
    fclose(second_bmp);
    free(initial_image.data);
    free(rotated_image.data);




    return 0;
}


